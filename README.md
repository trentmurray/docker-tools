# docker-tools

The dkc tool is a simple wrapper around docker-compose and docker-machine that allows you to run docker-compose commands
from anywhere on the command line based on values in the ~/.docker-tools/.config file.

## Basic setup

The following is a basic setup for dkc

```bash
git clone git@gitlab.com:trentmurray/docker-tools.git
cd docker-tools/
mkdir ~/.docker-tools/
cp dkc ~/.docker-tools/ && chmod 755 ~/.docker-tools/dkc
echo "alias dkc='~/.docker-tools/dkc'" >> ~/.bash_profile
. ~/.bash_profile

```

Once this basic setup is done, you'll need to configure your ~/.docker-tools/.config file with your specific
docker-machine and docker-compose project details.

## Usage of dkc:

### Supported commands:

run [options: -r (--rm)] [commands (space separated)]

up [options: -f (--force-recreate), -d (--detach), -nd (--no-deps), -b (--build)] [containers (space separated)]

build [options: -nc (--no-cache)] [containers (space separated)]

down

#### Running commands for a project configured in .docker-tools.env with a prefix CM
dkc -p CM <command> ...

#### Running a custom docker-file that is not the file configured in the .docker-tools.env
dkc -f dev.yml
